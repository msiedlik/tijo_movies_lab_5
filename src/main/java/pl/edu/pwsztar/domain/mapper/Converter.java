package pl.edu.pwsztar.domain.mapper;

interface Converter<T, U> {

  U convert(T data);
}
