package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieMapper implements Converter<CreateMovieDto, Movie> {

  @Override public Movie convert(CreateMovieDto data) {
    Movie movie = new Movie();

    movie.setImage(data.getImage());
    movie.setTitle(data.getTitle());
    movie.setYear(data.getYear());

    return movie;
  }
}
