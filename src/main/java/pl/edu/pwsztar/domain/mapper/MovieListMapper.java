package pl.edu.pwsztar.domain.mapper;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieListMapper implements Converter<List<Movie>, List<MovieDto>> {

  @Override public List<MovieDto> convert(List<Movie> data) {
    return data
        .stream()
        .map(movie -> {
          MovieDto movieDto = new MovieDto();
          movieDto.setMovieId(movie.getMovieId());
          movieDto.setTitle(movie.getTitle());
          movieDto.setImage(movie.getImage());
          movieDto.setYear(movie.getYear());
          return movieDto;
        })
        .collect(Collectors.toList());
  }
}
