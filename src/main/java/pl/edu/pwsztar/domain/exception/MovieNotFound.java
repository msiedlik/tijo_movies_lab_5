package pl.edu.pwsztar.domain.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(code = NOT_FOUND, reason = "Movie not found")
public class MovieNotFound extends RuntimeException {
}
