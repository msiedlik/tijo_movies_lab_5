package pl.edu.pwsztar.service;

import java.util.List;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieDto;

public interface MovieService {

    List<MovieDto> findAll();

    void createMovie(CreateMovieDto createMovieDto);

    void deleteMovie(Long movieId);
}
